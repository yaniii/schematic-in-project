// tslint:disable-next-line:class-name
export interface schemaOptions {
    name: string;
    project?: string;
    appRoot: string;
    path: string;
    sourceDir: string;
    service: boolean;
}
