import { schemaOptions } from '../schema';

export function getPathToModule(options: schemaOptions) {
    const spliceName = options.name.split('/');
    const moduleName = spliceName[spliceName.length - 1];
    const pathFile = `${options.path}/${options.name}/${moduleName}.module.ts`;

    return pathFile;

}
