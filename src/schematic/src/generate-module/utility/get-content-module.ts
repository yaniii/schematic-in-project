import { Tree } from '@angular-devkit/schematics';

export function getContenModule(tree: Tree, pathFile: string) {

    const content = tree.read(pathFile);

    const strContent = content ? content.toString() : '';

    return strContent;

}
