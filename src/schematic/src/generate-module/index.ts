import {
    chain,
    SchematicContext,
    Tree,
    externalSchematic,
    Rule,
} from '@angular-devkit/schematics';
import { classify } from '@angular-devkit/core/src/utils/strings';
import { schemaOptions } from './schema';
import { getContenModule } from './utility/get-content-module';
import { getPathToModule } from './utility/get-path-to-module';

export default function (options: schemaOptions): Rule {

    const spliceName = options.name.split('/');
    const moduleName = classify(spliceName[spliceName.length - 1]);

    return chain([
        externalSchematic('@schematics/angular', 'module', options),
        (tree: Tree, _context: SchematicContext) => {

            const moduleWithProviders = `\n
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: ${moduleName}Module,
            providers: [
            ]
        };
    }
`;
            const appendIndex = getContenModule(tree, getPathToModule(options)).lastIndexOf(`}`);
            const content2Append = moduleWithProviders;

            // tslint:disable-next-line:max-line-length
            const updatedContent = getContenModule(tree, getPathToModule(options)).slice(0, appendIndex) + content2Append + getContenModule(tree, getPathToModule(options)).slice(appendIndex);

            tree.overwrite(getPathToModule(options), updatedContent);

            return tree;
        },
        (tree: Tree, _context: SchematicContext) => {
            const content = tree.read(getPathToModule(options));

            const strContent = content ? content.toString() : '';

            const updatedContent = strContent.replace('NgModule', 'NgModule, ModuleWithProviders');

            tree.overwrite(getPathToModule(options), updatedContent);
            return tree;
        },
    ]);

}
