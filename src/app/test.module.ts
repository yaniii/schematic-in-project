import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
    imports: [
        CommonModule
    ]
})
export class Test Module {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: Test Module,
            providers: []
        };
    }
}
